import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import {
  Row,
  Container
} from 'reactstrap';

import san from '../../assets/img/san.jpg'
import blackuni from '../../assets/img/blackuni.jpg';
import  fox from '../../assets/img/fox.jpg'
import excup from '../../assets/img/excup.jpg'
import thistal from '../../assets/img/thistal.jpg'
import robin from '../../assets/img/robin.jpg'
import cvv from '../../assets/img/cvv.jpg'


export default class Shop extends Component {
  render() {
    const settings = {
      centerMode: true,
      infinite: true,
      speed: 800,
      slidesToShow: 4,
      autoplay: true,
      slidesToScroll: 4,
      initialSlide: 0,
      swipeToSlide: true,
      adaptiveHeight: true
    };
    return (
            <div className="responsiveCarousel containerWidthGallery container">
                <Row className="removeMarginRow">
                    <h5 className="setFeaturedProducts">
                        shop clothing
                    </h5>
                    <span className="seeAll">See all</span>
                </Row>
                <Slider {...settings}>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={san} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Retro Airline
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by ivankrpan
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                58 products
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={blackuni} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Black Unicorn
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Paula García
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                64 products
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={fox} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Winter Fox
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by DaleSimpson
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                21 products
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={cvv} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                colorful side road
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by sylvie  demers
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                46 products
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={thistal} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Thistle and moth
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Laorel
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                68 products
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={robin} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Winter Robin
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Anais Lee
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                26 products
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={excup} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                An existential cup of tea
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by zsalto
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                19 products
                            </div>
                        </Row>
                    </div>
                </Slider>
            </div>
    );
  }
}
