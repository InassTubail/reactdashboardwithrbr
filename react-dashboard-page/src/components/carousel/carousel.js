// import React, { useState } from 'react';
// import {
//   Carousel,
//   CarouselItem,
//   CarouselControl,
//   CarouselIndicators,
//   CarouselCaption
// } from 'reactstrap';

// const items = [
//   {
//     src: '../../../public/images/charles.jpg',
//     altText: 'Winter Sale',
//     caption: 'Winter Sale',
//     captionText: 'Don\'t miss the biggest sale of winter, starting on November 19th. '
//   },
//   {
//     src: '../../../public/images/clark.jpg',
//     altText: 'Winter Sale',
//     caption: 'Winter Sale 2019',
//     captionText: 'Don\'t miss the biggest sale of winter, starting on November 19th. '
//   },
//   {
//     src: '../../../public/images/charles.jpg',
//     altText: 'Winter Sale Woow',
//     caption: 'Hurry Up',
//     captionText: 'Don\'t miss the biggest sale of winter, starting on November 19th. '
//   }
// ];

// const carousel = (props) => {
//   const [activeIndex, setActiveIndex] = useState(0);
//   const [animating, setAnimating] = useState(false);

//   const next = () => {
//     if (animating) return;
//     const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
//     setActiveIndex(nextIndex);
//   };

//   const previous = () => {
//     if (animating) return;
//     const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
//     setActiveIndex(nextIndex);
//   };

//   const goToIndex = (newIndex) => {
//     if (animating) return;
//     setActiveIndex(newIndex);
//   };

//   const slides = items.map(item => (
//             <CarouselItem
//                 onExiting={() => setAnimating(true)}
//                 onExited={() => setAnimating(false)}
//                 key={item.src}
//             >
//                 <img src={item.src} className="setImage" alt={item.altText} />
//                 <CarouselCaption captionText={item.captionText} captionHeader={item.caption} />
//             </CarouselItem>
//   ));

//   return (
//     <Carousel
//         activeIndex={activeIndex}
//         next={next}
//         previous={previous}
//     >
//         <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
//         {slides}
//         {/* eslint-disable-next-line max-len */}
//         {/* <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} /> */}
//         {/* <CarouselControl direction="next" directionText="Next" onClickHandler={next} /> */}
//     </Carousel>
//   );
// };

// export default carousel;
