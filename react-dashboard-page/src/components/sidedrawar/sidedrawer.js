import React from 'react';
import {Link} from "react-router-dom";
import './sidebarDrawer.css';

const sideDrawer = props => (
    <nav className="side-drawer">
        <ul>
            <div className="side-drawer-login">
                <span>Hi, there</span>
                <br />
                <span>Login</span>
                <span className="distance-2-side-drawer">/</span>
                <span>Sign up</span>
            </div>
            <li>
                <Link className="nav-link" to="/home">Men's</Link>
            </li>
            <li>
                <Link className="nav-link" to="/home">Women's</Link>
            </li>
            <li>
                <Link className="nav-link" to="/home">Kids</Link>
            </li>
            <li>
                <Link className="nav-link" to="/home">Electronics</Link>
            </li>
            <li>
                <Link className="nav-link" to="/home">Accessories</Link>
            </li>
            <li>
                <Link className="nav-link" to="/home">Beauty & Care</Link>
            </li>
            <li>
                <Link className="nav-link" to="/home">Coming Soon</Link>
            </li>
        </ul>
    </nav>
);

export default sideDrawer;