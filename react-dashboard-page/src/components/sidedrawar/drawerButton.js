import React from 'react';
import listmenu from '../../assets/img/list-menu.png'

const drawerButton = props => (
    <div className="setDrawerButton">
        <img onClick={props.click} width={26} height={25} src={listmenu} />
    </div>
);

export default drawerButton;
