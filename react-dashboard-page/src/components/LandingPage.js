import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './home/Home';
import Contact from './contact/Contact';
import More from './more/More';
import './../stylesheet/Main.scss';
import Header from './Header/Head';
import Carousel from './carousel/carousel';
import Departments from './departments/departments';
import Gallery from './featured/gallery';
import Shop from './shop/singalSlide';
import Footer from './footer/footer';
import NavBar from './sidebar/sidebar';
import SideDrawer from './sidedrawar/sidedrawer';
import BackDrop from './sidedrawar/backDrop';

import 'bootstrap/dist/css/bootstrap.min.css';
import './../stylesheet/Main.scss';
import './../stylesheet/index.css';


export default class LandingPage extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.backDropHandler = this.backDropHandler.bind(this);
    this.state = {
      sideDrawerOpen: false
    };
  }
  toggle() {
    console.log('click');
    this.setState({
      sideDrawerOpen: !this.state.sideDrawerOpen
    });
  }

  backDropHandler() {
    console.log('close menu');
    this.setState({ sideDrawerOpen: false });
  }

  render() {
    let sideDrawer;
    let backDrop;

    if (this.state.sideDrawerOpen) {
      sideDrawer = <SideDrawer/>;
      backDrop = <BackDrop click={this.backDropHandler} />;
    }
    return (
        <Router>
          <div className="mainBg">
            <Header drawerClickHandler={this.toggle} />
            <NavBar />
            {sideDrawer}
            {backDrop}
            {/* <Carousel/> */}
            <br/>
            <br/>
            <main>
              <Departments/>
              <br/>
              <br/>
              <Gallery/>
              <br/>
              <br/>
              <Shop/>
              <br/>
              <br/>
              
            </main>
            <br/>
            <br/>
            <Footer/>
          </div>
        </Router>
    );
  }
}
