import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './sidebar.css';
import {
  Collapse,
  Nav, Navbar,
  NavItem
} from 'reactstrap';

export default class navbar extends Component {
  constructor(props) {
    console.log('side bar components area');
    console.log(props);
    super(props);
  }
  render() {
    return (
    <header className="Bg">
        <Navbar flex={2} light expand="md">
            {/* <div className="spacer"></div> */}
            <Collapse className="navbarCheck">
                <Nav className="mr-auto margin-auto" navbar>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/home">Men's</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/contact">Women's</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Kids</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Electronics</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Accessories</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Beauty & Care</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Coming Soon</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Coming Soon</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Coming Soon</Link>
                    </NavItem>
                    <NavItem className="distance">
                        <Link className="nav-link alignCenter" to="/more">Coming Soon</Link>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    </header>);
  }
}

