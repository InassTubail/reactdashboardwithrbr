import React, { Component } from 'react';
import { Input } from 'reactstrap';
import '../../stylesheet/Main.scss';
import Home from '../home/Home';
import { Link } from 'react-router-dom';
import DrawerButton from '../sidedrawar/drawerButton';
import SearchBox from './searchbox';
import SideDrawer from '../sidedrawar/sidedrawer';
import BackDrop from '../sidedrawar/backDrop';
import redbubble from'../../assets/img/redbubble.svg'
import shoppingbag from '../../assets/img/shopping-bag.png'
import search from'../../assets/img/search.svg'


export default class Head extends Component {
  constructor(props) {
    console.log(props);
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      searchBox: false
    };
  }
  toggle() {
    console.log('click');
    this.setState({ searchBox: !this.state.searchBox });
  }
  render() {
    let search;
    if (this.state.searchBox) {
      search = <SearchBox />;
    }
    return (
        <div>
          <div className="head">
              <div className="headInner">
                  <div className="logoArea">
                      <Link to="/home">
                          <img width={167.44} height={34} src={redbubble}/>
                      </Link>
                  </div>
                  <div className="searchBar">
                      <Input type="email" name="email" className="searchBarTextBox" id="exampleEmail"
                             placeholder="Search for items...."/>
                      {/* <img className="searchIcon" src="../../../public/search.svg" /> */}
                  </div>
                  <div className="selectBox">
                      <Input type="select" className="selectThing" name="select" id="exampleSelect">
                          <option selected="selected">All</option>
                          <option>men's</option>
                          <option>women's</option>
                      </Input>
                  </div>
                  <div className="loginSide">
                      <span>Login</span>
                      <span className="distance-2">/</span>
                      <span>Sign up</span>
                      <span className="ImageDistance">
                        <img src={shoppingbag} className="shoppingBag"/>
                    </span>
                  </div>
              </div>
              <div className="responsiveHeader">
                  <div className="headResponse">
                      <div className="responsiveData">
                          <DrawerButton click={this.props.drawerClickHandler}/>
                          <div className="searchIcon" onClick={this.toggle}>
                              <img width={24} height={24} src={search}/>
                          </div>
                          <div className="logoResponsive">
                              <Link to="/home">
                                  <img className="respImage" width={130.44} height={34} src={redbubble}/>
                              </Link>
                          </div>
                          <Input type="select" className="selectThing" name="select" id="exampleSelect">
                              <option selected="selected">All</option>
                              <option>men's</option>
                              <option>women's</option>
                          </Input>
                      </div>
                  </div>
                  {search}
              </div>
          </div>
      </div>
    );
  }
}

