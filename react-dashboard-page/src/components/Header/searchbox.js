import React from 'react';
import { Input } from 'reactstrap';
// import search from'../../assets/img/search.svg'

const sideDrawer = props => (
    <div className="searchBarResponsive">
        <Input type="email" name="email" className="searchBarTextBox" id="exampleEmail"
               placeholder="Search for items...."/>
        {/* <img className="searchIcon" src={search} /> */}
    </div>
);

export default sideDrawer;
