import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import {
  Row,
  Container,
  Col
} from 'reactstrap';
import redb1 from'../../assets/img/redb1.jpg'
import mobile from'../../assets/img/mobile.jpg'
import bag from'../../assets/img/bag.jpg'
import blue from'../../assets/img/blue.jpg'
import shirt from'../../assets/img/shirt.jpg'
import tshit from'../../assets/img/tshit.jpg'
import tshirt1 from'../../assets/img/tshirt1.jpg'
import tcup from'../../assets/img/tcup.jpg'
import pink from'../../assets/img/pink.jpg'







export default class MultipleItems extends Component {
  render() {
    const settings = {
      centerMode: true,
      infinite: true,
      speed: 800,
      rows: 2,
      autoplay: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      swipeToSlide: true,
      initialSlide: 0,
      adaptiveHeight: true
    };
    return (
            <div className="responsiveGallery containerWidthGallery container">
                <h5 className="setFeaturedProducts"> Featured Products </h5>
                <Slider {...settings}>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={redb1}/>
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Ornate Buck
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by BioWorkZ
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $2.81
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={mobile} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Abstract Patterns
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by KateMerrittshop
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $25.25
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={bag} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Eurasian Kestrels
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Scott Partridge
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $20.21
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={bag} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Snowy Triceratops
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by salami-spots
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $17.07
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={blue} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Blue Whale
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Amber Davenport
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $21.01
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={shirt} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Winter Vibes
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Hillary White
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $20.94
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={tshit} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Black Unicorn
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Paula García
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $29.27
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={tcup} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Tabby to look up at
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Toru Sanogawa
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $25.25
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={tshirt1} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Thistle and moth
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by Laorel
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $20.94
                            </div>
                        </Row>
                    </div>
                    <div className="containerBox">
                        <Row className="adjustRow">
                            <img src={pink} />
                        </Row>
                        <Row className="adjustRowN">
                            <div className="productName">
                                Pink cones.
                            </div>
                        </Row>
                        <Row className="adjustRowA">
                            <div className="productAuthor">
                                by smalldrawing
                            </div>
                        </Row>
                        <Row className="adjustRowP">
                            <div className="productPrice">
                                $22.95
                            </div>
                        </Row>
                    </div>
                </Slider>
            </div>
    );
  }
}
