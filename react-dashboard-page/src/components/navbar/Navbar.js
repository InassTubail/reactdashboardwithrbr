import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem
} from 'reactstrap';
import { Link } from 'react-router-dom';
import '../../stylesheet/Main.scss';

export default class NavBar extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div className="Bg">
        <Navbar flex={2} light expand="md">
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="mr-auto margin-auto" navbar>
              <NavItem className="distance">
                <Link className="nav-link alignCenter" to="/home">Men's</Link>
              </NavItem>
              <NavItem className="distance">
                <Link className="nav-link alignCenter" to="/contact">Women's</Link>
              </NavItem>
              <NavItem className="distance">
              <Link className="nav-link alignCenter" to="/more">Kids</Link>
              </NavItem>
              <NavItem className="distance">
                <Link className="nav-link alignCenter" to="/more">Electronics</Link>
              </NavItem>
              <NavItem className="distance">
                <Link className="nav-link alignCenter" to="/more">Accessories</Link>
              </NavItem>
              <NavItem className="distance">
                <Link className="nav-link alignCenter" to="/more">Beauty & Care</Link>
              </NavItem>
              <NavItem className="distance">
                <Link className="nav-link alignCenter" to="/more">Coming Soon</Link>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
