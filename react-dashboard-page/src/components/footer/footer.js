import React from 'react';
import {
  Row,
  Col,
  Container
} from 'reactstrap';
import '../../stylesheet/Main.scss';
import insta from'../../assets/img/insta.png'
import fb from'../../assets/img/fb.png'
import tweet from'../../assets/img/tweet.png'
import redbubble from'../../assets/img/redbubble.svg'


import { Link } from 'react-router-dom';

const Footer = () => (
        <Container className="Footer">
            <div className="FooterInner">
                <Row className="rowOne">
                    <Col className="Col">
                        <h6 className="itSelf">Shop</h6>
                        <Link to="" className="spanShop">Gift Guides</Link>
                        <Link to="" className="spanShop">Login</Link>
                        <Link to="" className="spanShop">Sign Up</Link>
                    </Col>
                    <Col className="Col">
                        <h6 className="itSelf">About</h6>
                        <Link to="" className="spanShop">About Us</Link>
                        <Link to="" className="spanShop">Social Responsibility</Link>
                        <Link to="" className="spanShop">Jobs</Link>
                    </Col>
                    <Col className="Col">
                        <h6 className="itSelf">Help</h6>
                        <Link to="" className="spanShop">Delivery</Link>
                        <Link to="" className="spanShop">Returns</Link>
                        <Link to="" className="spanShop">Help Center</Link>
                        <Link to="" className="spanShop">Copyright</Link>
                        <Link to="" className="spanShop">Contact Us</Link>
                    </Col>
                    <Col className="Col">
                        <h6 className="itSelf">Social</h6>
                        <Link to="" className="spanShop linkPadding">
                            <img className="socialMediaIcon" src={insta} />
                            Instagram
                        </Link>
                        <Link to="" className="spanShop linkPadding">
                            <img className="socialMediaIcon" src={fb} />
                            Facebook
                        </Link>
                        <Link to="" className="spanShop linkPadding">
                            <img className="socialMediaIcon" src={tweet} />
                            Twitter
                        </Link>
                    </Col>
                </Row>
            </div>
            <div className="FooterInnerBottom">
                <Row className="footerRow">
                    <Col className="ColMd6">
                        <img className="BottomLogo" width={184.44} height={34} src={redbubble} />
                    </Col>
                    <Col className="ColMd6">
                        <Link to="" className="footerBottomLink">User Agreement</Link>
                        <Link to="" className="footerBottomLink">Privacy Policy</Link>
                        <Link to="" className="footerBottomLink">Redbubble uses cookies: Cookie Policy</Link>
                    </Col>
                </Row>
            </div>
            <div className="footerBottomSection">
                <Row className="rightsReserved">
                    <span>© Redbubble. All Rights Reserved</span>
                </Row>
            </div>
        </Container>
);

export default Footer;
