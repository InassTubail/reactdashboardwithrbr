import React from 'react';
import {
    Row,
    Container,
    Col
} from 'reactstrap';

import hh from '../../assets/img/team-1-800x800.jpg'
import red3 from "../../assets/img/red3.jpg"
import red2 from  "../../assets/img/red2.jpg"
import red5 from  "../../assets/img/red5.jpg"
import red7 from  "../../assets/img/red7.jpg"


export default function departments() {
  return (
            <div className="departments">
                <h3 className="h_departments">Departments</h3>
                <br />
                <br />
                <Container className="containerWidth">
                    <Row>
                        <Col className="set_departments" >
                            <img className="image_size" src={hh} />
                            <span className="set_departments_text">Men's</span>
                        </Col>
                        <Col className="set_departments" >
                            <img className="image_size" src={red3} />
                            <span className="set_departments_text">Women's</span>
                        </Col>
                        <Col className="set_departments" >
                            <img className="image_size" src={red2} />
                            <span className="set_departments_text">Accessories's</span>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="set_departments_2" >
                            <img className="image_size_2" src={red5} />
                            <span className="set_departments_text_2">Kid's</span>
                        </Col>
                        <Col className="set_departments_2" >
                            <img className="image_size_2" src={red7} />
                            <span className="set_departments_text_2">Beauty & Care</span>
                        </Col>
                    </Row>
                </Container>
            </div>
  );
}
